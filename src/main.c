#include <assert.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#define ANOTHER_MMAP_ADDR ((void*)0x04045000)

static void print_msg(FILE* f, const bool condition, const size_t test, const size_t step) {
	if (condition) {
		fprintf( f, "%s", "PASSED\n");
		fprintf( f, "%s", "*********************\n");
	} else {
		fprintf( f, "%s", "FAILED\n");
		fprintf( f, "%s", "*********************\n");
		err("FAIL in test %zu step %zu\n", test, step);
	}
}

static void* step_test_log(const size_t query, const void* heap, FILE* f) {
	fprintf( f, "%s", "*********************\n");
	fprintf( f, "%s", "Initial heap state:\n");
	debug_heap(f,  heap);
	void* test_block = _malloc(query);
	fprintf( f, "%s", "*********************\n");
	fprintf( f, "%s", "Result heap state:\n");
	debug_heap(f,  heap);
	fprintf( f, "%s", "*********************\n");
	return test_block;
}

static void print_test_header(const char* header, FILE* f) {
	fprintf( f, "%s", "*********************************************\n");
	fprintf( f, "%s", header);
	fprintf( f, "%s", "*********************************************\n");
}

void test_free_single_block(FILE* f) {
	print_test_header("Test 1 (free single block) started!\n", f);
	void* block0 = _malloc(5);
	void* block1 = _malloc(55);
	void* block2 = _malloc(55);
	void* test_block;
	fprintf( f, "%s", "Step 1 - one block is freed:\n");
	_free(block1);
	test_block = _malloc(55);
	bool not_null = (block0) && (block1) && (block2) && (test_block);
	print_msg(f, (test_block == block1) && not_null, 1, 1);
	_free(block0);
	_free(test_block);
	_free(block2);
}

void test_free_multiple_blocks(FILE* f) {
	print_test_header("Test 2 (free multiple blocks) started!\n", f);
	void* block0 = _malloc(4);
	void* block1 = _malloc(55);
	void* block2 = _malloc(55);
	void* block3 = _malloc(55);
	void* test_block;
	bool not_null = (block0) && (block1) && (block2) && (block3);
	fprintf( f, "%s", "Step 1 - three consequent blocks are freed:\n");
	_free(block1);
	_free(block2);
	_free(block3);
	test_block = _malloc(155);
	not_null = not_null && test_block;
	print_msg(f, (test_block == block1) && not_null, 2, 1);
	_free(block0);
	_free(test_block);
}

void test_malloc(const void* heap, FILE* f) {
	print_test_header("Test 3 (malloc) started!\n", f);
	void* block0 = _malloc(4); // will be rounded to 24
	void* block1 = _malloc(55);
	void* block2 = _malloc(55);
	void* block3 = _malloc(55);
	void* test_block;
	bool not_null = (block0) && (block1) && (block2) && (block3);
	fprintf( f, "%s", "Step 1 - trying to fill the gap (query < gap, but gap is not big enough to split it):\n");
	_free(block1);
	test_block = step_test_log(54, heap, f);
	print_msg(f, test_block == block1 && test_block != NULL && not_null, 3, 1);
	fprintf( f, "%s", "Step 2 - unable to fill the gap (query > gap, so the gap cannot be filled):\n");
	_free(test_block);
	test_block = step_test_log(56, heap, f);
	print_msg(f, test_block != block1 && test_block != NULL && not_null, 3, 2);
	fprintf( f, "%s", "Step 3 - trying to fill the double gap (query < double gap and gap is big enough to be splitted):\n");
	_free(test_block);
	_free(block2);
	test_block = step_test_log(56, heap, f);
	print_msg(f, test_block == block1 && test_block != NULL && not_null, 3, 3);
	_free(block0);
	_free(test_block);
	_free(block3);
}

void test_grow_heap_adjacent(const void* heap, FILE* f) {
	print_test_header("Test 4 (grow heap adjacent) started!\n", f);
	fprintf( f, "%s", "Step 1:\n");
	void* block0 = step_test_log(10000, heap, f);
	const bool condition = block0 && (block0 == ((int8_t*) HEAP_START) + offsetof( struct block_header, contents )); //10000 > initial heap size, but the first block starts at heap start => extra memory allocated just after the initial block
	print_msg(f, condition, 4, 1); 
	_free(block0);
}

void test_grow_heap_remote(const void* heap, FILE* f) {
	print_test_header("Test 5 (grow heap remote) started!\n", f);
	fprintf( f, "%s", "Step 1:\n");
	void* p = mmap( (void*) ANOTHER_MMAP_ADDR, 4096*2, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0 );
	fprintf(f, "HEAP START POINTER: %p\n", HEAP_START);
	fprintf(f, "NEW MMAP POINTER: %p\n", p);
	void* block0 = step_test_log(32000, heap, f);
	const bool condition = block0 && (block0 != ((int8_t*) HEAP_START) + offsetof( struct block_header, contents )); // block was allocated, but not at the start of the heap => new mapping was placed somewhere else
	print_msg(f, condition, 5, 1); 
	_free(block0);
}

int main() {
	const void* heap = heap_init(1);
	test_free_single_block(stdout);
	test_free_multiple_blocks(stdout);
	test_malloc(heap, stdout);
	test_grow_heap_adjacent(heap, stdout);
	test_grow_heap_remote(heap, stdout);
	return 0;
}

